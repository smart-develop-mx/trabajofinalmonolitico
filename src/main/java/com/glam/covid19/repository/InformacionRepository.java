package com.glam.covid19.repository;
 

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.glam.covid19.model.Informacion;
 
 
@Repository
public interface InformacionRepository extends JpaRepository<Informacion,Integer> {
	 
}
