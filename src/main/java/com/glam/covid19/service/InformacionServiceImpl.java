package com.glam.covid19.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.glam.covid19.model.Informacion;
import com.glam.covid19.repository.InformacionRepository;


@Service
public class InformacionServiceImpl implements InformacionService{

	@Autowired
	private InformacionRepository informacionRepository;
	
	@Override
	public List<Informacion> findAll() {
		return informacionRepository.findAll(); 
	}

	@Override
	public Optional<Informacion> findById(Integer id) { 
		return informacionRepository.findById(id);
	}

	@Override
	public Informacion save(Informacion t) {
		return  informacionRepository.save(t);
	}
	

}
