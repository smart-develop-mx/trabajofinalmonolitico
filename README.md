# Aplicación Monolítica
_Este proyecto es una aplicación de tipo Monolótica para el curso Arquitecto DevOps - mitocode._


## Objetivo
_Este proyecto tiene como finalidad aplicar lo aprendido en el curso, mediante la implementación de Integracón continua, Entrega continua y Despliegue Continuo_


## Pre-requisitos
_Se requerie acceso a:_ 

* [Jenkins](http://143.198.49.111:8080/)
* [Sonarqube](http://143.198.49.111:9000/projects)
* [JFrog Artifactory](http://143.198.49.111:8082/ui/)
* [Jboss](http://143.198.49.111:10990/)(opcional)

_Repositorio Público_

* [Bitbucket](https://bitbucket.org/smart-develop-mx/trabajofinalmonolitico)


## Ejecutar pipeline
_Para poder ejecutar el job, se debe ingresar al servidor de_ [Jenkins](http://143.198.49.111:8080/)_, el unico job con el nombre de monolitico, dar clic en la opción construir ahora.

_Fases:_

* Build
* Testing
* Sonar 
* Artifactory 
* Despliegue en Jboss 7.2


## Artifactory
Para el alojamiento de los artefactos en [JFrog Artifactory](http://143.198.49.111:8082/ui/), se maneja la siguiente lógica.
La rama master se alojará en el repositorio release

## Sonarqube
_Para visualizar el reporte, se debe ingresar a_ [Sonarqube](http://143.198.49.111:9000)

_También se puede observar la cobertura  en jenkins en la sección tendencia de cobertura._


## Jboss 
_Si se requiere validar el artefacto desplegado se debe de ingresar a: [Jboss](http://143.198.49.111:10990/) y seleccionar deployments_

 

## Autor
* **JOSE GILBERTO COLIN** - *Alumno* 
 
 
